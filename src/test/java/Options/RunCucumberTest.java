package Options;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions( tags = "@smoke",
        plugin = {"pretty"},
        glue = {"stepdefs"},
        features = {"src/test/features"})
public class RunCucumberTest {

}
