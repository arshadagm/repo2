package stepdefs;

import io.cucumber.java.en.Given;
import static io.restassured.RestAssured.*;
//import static org.hamcrest.Matchers.is;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.hamcrest.CoreMatchers.is;
public class GetPostSteps
{
    @Given("I perform GET operation for Test")
    public void iPerformGETOperationForTest()
    {
        given().get("http://ip.jsontest.com/").
                then().body("ip",is("106.208.88.149")).statusCode(200);
        //System.out.println();
        //WebDriver driver = new ChromeDriver();


    }

}
